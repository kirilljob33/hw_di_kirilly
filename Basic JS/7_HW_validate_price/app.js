

// При загрузке страницы показать пользователю поле ввода (input) с надписью Price.
// Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
//
//   При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
//   Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст:
//   Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X).
//   Значение внутри поля ввода окрашивается в зеленый цвет.
//   При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
//   Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу
//  - Please enter correct price. span со значением при этом не создается.

document.body.insertAdjacentHTML('afterbegin', '<div id = "parent-box"><input id = "parentInput" type="text" placeholder = "Price"></input></div>')

function focus() {
    let getElement = document.getElementById('parentInput')
    getElement.onfocus = function () {
        getElement.classList.toggle('parent-input-green')
    }

    getElement.onblur = function () {
        let checker = document.getElementById('currentPrice')
        getElement.classList.toggle('parent-input-green')
        let getParent = document.getElementById('parent-box')
        if (getElement.value > null) {
            getElement.style.color = 'green'
            if(checker === null){
                getParent.insertAdjacentHTML('afterbegin', `<span id="currentPrice">Current price: ${getElement.value}<button id = "cancelPrice">x</button></span>`)
            }else checker.innerHTML = `Current price: ${getElement.value}<button id = "cancelPrice">x</button>`
            let cancelPrice = document.getElementById('cancelPrice')
            cancelPrice.addEventListener('click', () => cancelPrice.parentNode.remove())

            function remover() {
                let removeWarning = document.getElementById('warning')
                if (removeWarning !== null) {
                    removeWarning.parentNode.removeChild(removeWarning)
                    getElement.classList.remove('parent-input-red')
                }

            }
            remover()
        } else {

            let warning = document.getElementById('warning')

            if (warning === null && getElement.value !== '') {
                getElement.style.color = 'red'
                getElement.classList.add('parent-input-red')
                getParent.insertAdjacentHTML('beforeend', `<span id="warning">Please enter only numbers and bigger then null</span>`)
            }
            return
        }

    }
}

focus()



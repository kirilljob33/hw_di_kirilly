
// Считать с помощью модального окна браузера число, которое введет пользователь.
//     Вывести в консоли все числа кратные 5, от 0 до введенного пользователем числа.
//     Если таких чисел нету - вывести в консоль фразу `Sorry, no numbers'
// Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.



let userNumber = +prompt('Please enter your number')

while (!userNumber){
    userNumber = prompt('Enter your number once again')
}

for (let counter = 0; counter <= userNumber; counter++) {
    if (counter % 5 === 0) {
        console.log(counter)
    } else if (userNumber < 5) {
        alert('Sorry, no numbers')
        break
    }
}




// Считать с помощью модельного окна браузера данные пользователя: имя и возраст.
//     Если возраст меньше 18 лет - показать на экране сообщение: You are not allowed to visit this website.
//     Если возраст от 18 до 22 лет (включительно) - показать окно со следующим сообщением: Are you sure you want to continue? и кнопками Ok, Cancel.
//     Если пользователь нажал Ok, показать на экране сообщение: Welcome,  + имя пользователя.
//     Если пользователь нажал Cancel, показать на экране сообщение: You are not allowed to visit this website.
//     Если возраст больше 22 лет - показать на экране сообщение: Welcome,  + имя пользователя.
//     Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.


let userName = prompt('Enter your name', 'Kira')
let userAge = +prompt('Enter your age')

if(userAge < 18){
    alert('You are not allowed to visit this website')
} else if (userAge >= 18 && userAge < 22) {
    answer = confirm('Are you sure you want to continue?')
    if(answer === true){
        alert(`Welcome,  ${userName}.`)
    } else{
        alert('You are not allowed to visit this website')
    }
} else if(userAge > 22){
    alert(`Welcome,  ${userName}.`)
}


